package id.ramdannur.kamus.helpers.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;

import id.ramdannur.kamus.entities.Translation;

import static android.provider.BaseColumns._ID;
import static id.ramdannur.kamus.helpers.database.DatabaseContract.TABLE_NAME;
import static id.ramdannur.kamus.helpers.database.DatabaseContract.TranslationColumns.DESCRIPTION;
import static id.ramdannur.kamus.helpers.database.DatabaseContract.TranslationColumns.NAME;
import static id.ramdannur.kamus.helpers.database.DatabaseContract.TranslationColumns.TYPE;

/**
 * Created by Ramdannur on 1/29/2019.
 */
public class TranslationHelper {
    private Context context;
    private DatabaseHelper dataBaseHelper;

    private SQLiteDatabase database;

    public TranslationHelper(Context context) {
        this.context = context;
    }

    public TranslationHelper open() throws SQLException {
        dataBaseHelper = new DatabaseHelper(context);
        database = dataBaseHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dataBaseHelper.close();
    }

    public ArrayList<Translation> getDataByType(String type) {
        Cursor cursor = database.query(
                TABLE_NAME,
                null,
                TYPE + " = ?",
                new String[]{type},
                null,
                null,
                _ID + " ASC",
                null);

        cursor.moveToFirst();
        ArrayList<Translation> arrayList = new ArrayList<>();
        Translation translationModel;
        if (cursor.getCount() > 0) {
            do {
                translationModel = new Translation();
                translationModel.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                translationModel.setName(cursor.getString(cursor.getColumnIndexOrThrow(NAME)));
                translationModel.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(DESCRIPTION)));
                translationModel.setType(cursor.getString(cursor.getColumnIndexOrThrow(TYPE)));

                arrayList.add(translationModel);
                cursor.moveToNext();

            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public Translation findById(int id) {
        Translation translation = null;
        Cursor cursor = database.query(
                TABLE_NAME,
                null,
                _ID + " = ?",
                new String[]{String.valueOf(id)},
                null,
                null,
                _ID + " ASC",
                null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            translation = new Translation();
            translation.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
            translation.setName(cursor.getString(cursor.getColumnIndexOrThrow(NAME)));
            translation.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(DESCRIPTION)));
            translation.setType(cursor.getString(cursor.getColumnIndexOrThrow(TYPE)));
        }

        cursor.close();

        return translation;
    }

    public ArrayList<Translation> getAllData() {
        Cursor cursor = database.query(TABLE_NAME, null, null, null, null, null, _ID + " ASC", null);
        cursor.moveToFirst();
        ArrayList<Translation> arrayList = new ArrayList<>();
        Translation translationModel;
        if (cursor.getCount() > 0) {
            do {
                translationModel = new Translation();
                translationModel.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                translationModel.setName(cursor.getString(cursor.getColumnIndexOrThrow(NAME)));
                translationModel.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(DESCRIPTION)));
                translationModel.setType(cursor.getString(cursor.getColumnIndexOrThrow(TYPE)));

                arrayList.add(translationModel);
                cursor.moveToNext();


            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public long insert(Translation translationModel) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(NAME, translationModel.getName());
        initialValues.put(DESCRIPTION, translationModel.getDescription());
        initialValues.put(TYPE, translationModel.getType());
        return database.insert(TABLE_NAME, null, initialValues);
    }

    public void beginTransaction() {
        database.beginTransaction();
    }

    public void setTransactionSuccess() {
        database.setTransactionSuccessful();
    }

    public void endTransaction() {
        database.endTransaction();
    }

    public void insertTransaction(Translation translationModel) {
        String sql = "INSERT INTO " + TABLE_NAME + " (" + NAME + ", " + DESCRIPTION + ", " + TYPE
                + ") VALUES (?, ?, ?)";
        SQLiteStatement stmt = database.compileStatement(sql);
        stmt.bindString(1, translationModel.getName());
        stmt.bindString(2, translationModel.getDescription());
        stmt.bindString(3, translationModel.getType());
        stmt.execute();
        stmt.clearBindings();

    }

    public int update(Translation translationModel) {
        ContentValues args = new ContentValues();
        args.put(NAME, translationModel.getName());
        args.put(DESCRIPTION, translationModel.getDescription());
        args.put(TYPE, translationModel.getType());
        return database.update(TABLE_NAME, args, _ID + "= '" + translationModel.getId() + "'", null);
    }


    public int delete(int id) {
        return database.delete(TABLE_NAME, _ID + " = '" + id + "'", null);
    }
}
