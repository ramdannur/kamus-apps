package id.ramdannur.kamus.helpers.database;

import android.provider.BaseColumns;

/**
 * Created by Ramdannur on 1/29/2019.
 */
public class DatabaseContract {

    static String TABLE_NAME = "table_translations";

    static final class TranslationColumns implements BaseColumns {
        static String NAME = "name";
        static String DESCRIPTION = "description";
        static String TYPE = "type";
    }
}