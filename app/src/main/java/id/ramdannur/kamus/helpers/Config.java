package id.ramdannur.kamus.helpers;

/**
 * Created by Ramdannur on 1/28/2019.
 */
public class Config {
    public static final String APP_FIRST_RUN = "app_first_run";
    public static final String EN_LANG = "en";
    public static final String ID_LANG = "id";
    public static final String KEY_ARGS = "args";
    public static final String KEY_ID = "id";
}
