package id.ramdannur.kamus.helpers.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static android.provider.BaseColumns._ID;
import static id.ramdannur.kamus.helpers.database.DatabaseContract.TABLE_NAME;
import static id.ramdannur.kamus.helpers.database.DatabaseContract.TranslationColumns.DESCRIPTION;
import static id.ramdannur.kamus.helpers.database.DatabaseContract.TranslationColumns.NAME;
import static id.ramdannur.kamus.helpers.database.DatabaseContract.TranslationColumns.TYPE;

/**
 * Created by Ramdannur on 1/29/2019.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static String DATABASE_NAME = "dbmahasiswa";

    private static final int DATABASE_VERSION = 1;

    public static String CREATE_TABLE_MAHASISWA = "create table " + TABLE_NAME +
            " (" + _ID + " integer primary key autoincrement, " +
            NAME + " text not null, " +
            DESCRIPTION + " text not null, " +
            TYPE + " text not null);";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_MAHASISWA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}