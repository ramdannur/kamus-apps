package id.ramdannur.kamus.ui.list;

import java.util.List;

import id.ramdannur.kamus.entities.Translation;

/**
 * Created by Ramdannur on 1/29/2019.
 */
public interface TranslationListener {
    void showTranslations(List<Translation> translations);
}
