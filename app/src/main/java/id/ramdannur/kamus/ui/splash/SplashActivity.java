package id.ramdannur.kamus.ui.splash;

import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ramdannur.kamus.R;
import id.ramdannur.kamus.entities.Translation;
import id.ramdannur.kamus.helpers.AppPreference;
import id.ramdannur.kamus.helpers.Config;
import id.ramdannur.kamus.helpers.database.TranslationHelper;
import id.ramdannur.kamus.ui.main.MainActivity;

public class SplashActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();

    @BindView(R.id.progress_horizontal)
    ProgressBar progressBar;

    @BindView(R.id.tv_information)
    TextView tvInformation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        new LoadData(Config.EN_LANG).execute();
    }

    private class LoadData extends AsyncTask<Void, Integer, Void> {
        TranslationHelper translationHelper;
        AppPreference appPreference;
        double progress;
        double maxprogress = 100;
        String lang;

        LoadData(String lang) {
            this.lang = lang;
        }

        @Override
        protected void onPreExecute() {

            translationHelper = new TranslationHelper(SplashActivity.this);
            appPreference = new AppPreference(SplashActivity.this);

            progress = 0;
            publishProgress((int) progress);

            if (lang.equals(Config.EN_LANG))
                tvInformation.setText(R.string.prepare_english_data);
            else
                tvInformation.setText(R.string.prepare_indonesian_data);
        }

        @Override
        protected Void doInBackground(Void... params) {

            Boolean firstRun = appPreference.getFirstRun();

            if (firstRun) {

                ArrayList<Translation> translations = preLoadRaw(lang);

                translationHelper.open();

                progress = 30;
                publishProgress((int) progress);
                Double progressMaxInsert = 80.0;
                Double progressDiff = (progressMaxInsert - progress) / translations.size();

                translationHelper.beginTransaction();

                try {
                    for (Translation model : translations) {
                        translationHelper.insertTransaction(model);
                        progress += progressDiff;
                        publishProgress((int) progress);
                    }
                    // Jika semua proses telah di set success maka akan di commit ke database
                    translationHelper.setTransactionSuccess();
                } catch (Exception e) {
                    // Jika gagal maka do nothing
                    e.printStackTrace();
                }
                translationHelper.endTransaction();

                translationHelper.close();

                publishProgress((int) maxprogress);

            } else {
                try {
                    synchronized (this) {
                        this.wait(1500);

                        publishProgress(50);

                        this.wait(1500);
                        publishProgress((int) maxprogress);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progressBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void result) {
            if (lang.equals(Config.EN_LANG)) {
                new LoadData(Config.ID_LANG).execute();
            } else {
                appPreference.setFirstRun(false);
                tvInformation.setText(R.string.finished);
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }
    }

    public ArrayList<Translation> preLoadRaw(String lang) {
        ArrayList<Translation> translations = new ArrayList<>();
        String line;
        BufferedReader reader;
        try {
            Resources res = getResources();

            int rawFile;
            if (lang.equals(Config.EN_LANG))
                rawFile = R.raw.english_indonesia;
            else
                rawFile = R.raw.indonesia_english;

            InputStream raw_dict = res.openRawResource(rawFile);

            reader = new BufferedReader(new InputStreamReader(raw_dict));
            do {
                line = reader.readLine();
                String[] splitstr = line.split("\t");
                Translation translation;

                translation = new Translation(splitstr[0], splitstr[1], lang);
                translations.add(translation);
            } while (line != null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return translations;
    }

}
