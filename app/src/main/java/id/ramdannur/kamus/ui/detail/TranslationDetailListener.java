package id.ramdannur.kamus.ui.detail;

import id.ramdannur.kamus.entities.Translation;

/**
 * Created by Ramdannur on 1/30/2019.
 */
public interface TranslationDetailListener {
    void showTranslation(Translation translation);
}
