package id.ramdannur.kamus.ui.detail;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ramdannur.kamus.R;
import id.ramdannur.kamus.entities.Translation;
import id.ramdannur.kamus.helpers.Config;
import id.ramdannur.kamus.ui.base.BaseActivity;

public class TranslationDetailActivity extends BaseActivity implements TranslationDetailListener {

    private TranslationDetailPresenter presenter;
    private int translationId;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_description)
    TextView tvDescription;

    public static Intent getIntent(Activity act, int id) {
        Bundle bundle = new Bundle();
        Intent intent = new Intent(act, TranslationDetailActivity.class);
        bundle.putInt(Config.KEY_ID, id);
        intent.putExtras(bundle);
        return intent;
    }

    private void parseIntent() {
        Intent dataIntent = getIntent();

        if (dataIntent != null) {
            Bundle bundle = dataIntent.getExtras();
            if (bundle != null) {
                if (bundle.containsKey(Config.KEY_ID)) {
                    translationId = bundle.getInt(Config.KEY_ID);
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translation_detail);
        ButterKnife.bind(this);

        init();

        parseIntent();

        presenter = new TranslationDetailPresenter(this, getApplicationContext());
        presenter.getData(translationId);
    }

    private void init() {
        setToolbarBack(toolbar);
    }

    @Override
    public void showTranslation(Translation translation) {
        tvTitle.setText(translation.getName());
        tvDescription.setText(translation.getDescription());
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
