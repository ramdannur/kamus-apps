package id.ramdannur.kamus.ui.detail;

import android.content.Context;

import id.ramdannur.kamus.helpers.database.TranslationHelper;

/**
 * Created by Ramdannur on 1/30/2019.
 */
class TranslationDetailPresenter {

    private final TranslationDetailListener view;
    private TranslationHelper translationHelper;

    TranslationDetailPresenter(TranslationDetailListener view, Context context) {
        this.view = view;
        translationHelper = new TranslationHelper(context);
        translationHelper.open();
    }

    void getData(int id) {
        view.showTranslation(translationHelper.findById(id));
    }

    void onDestroy() {
        translationHelper.close();
    }
}
