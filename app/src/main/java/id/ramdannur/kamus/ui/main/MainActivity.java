package id.ramdannur.kamus.ui.main;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.ramdannur.kamus.R;
import id.ramdannur.kamus.helpers.Config;
import id.ramdannur.kamus.ui.base.BaseActivity;
import id.ramdannur.kamus.ui.list.TranslationFragment;

public class MainActivity extends BaseActivity implements MainListener {

    private MainPresenter presenter;
    private final int TAB_EN = 0;
    private final int TAB_ID = 1;
    private ViewPagerAdapter adapter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @BindView(R.id.pager)
    ViewPager viewPager;

    @BindView(R.id.txt_search)
    EditText txtSearch;

    @OnClick(R.id.btn_search)
    void onSearch() {
        searchTranslation();
    }

    private void searchTranslation() {
        String varKeyword = txtSearch.getText().toString().trim();
        TranslationFragment activeFragment = adapter.getPrimaryItem();
        if (activeFragment != null)
            activeFragment.filter(varKeyword);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        init();

        presenter = new MainPresenter(this);
    }

    private void init() {
        setToolbarHome(toolbar);

        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.setCurrentItem(TAB_EN);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                searchTranslation();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    // Adapter for the viewpager using FragmentPagerAdapter
    class ViewPagerAdapter extends FragmentStatePagerAdapter {

        TranslationFragment current;

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            String type = "";
            switch (position) {
                case TAB_EN:
                    type = Config.EN_LANG;
                    break;

                case TAB_ID:
                    type = Config.ID_LANG;
                    break;
            }
            return TranslationFragment.newInstance(type);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            if (current != object) {
                current = (TranslationFragment) object;
            }
            super.setPrimaryItem(container, position, object);
        }

        TranslationFragment getPrimaryItem() {
            return current;
        }

        @Override
        public int getCount() {
            return 2; // number page
        }

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case TAB_EN:
                    return getString(R.string.english);
                case TAB_ID:
                    return getString(R.string.indonesian);
                default:
                    return null;
            }
        }
    }
}
