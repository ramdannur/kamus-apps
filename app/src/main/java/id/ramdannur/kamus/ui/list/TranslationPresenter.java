package id.ramdannur.kamus.ui.list;

import android.content.Context;

import id.ramdannur.kamus.helpers.database.TranslationHelper;

/**
 * Created by Ramdannur on 1/29/2019.
 */
class TranslationPresenter {

    private final TranslationListener view;
    private TranslationHelper translationHelper;

    TranslationPresenter(TranslationListener view, Context context) {
        this.view = view;
        translationHelper = new TranslationHelper(context);
        translationHelper.open();
    }

    void getData(String type) {
        view.showTranslations(translationHelper.getDataByType(type));
    }

    void onDestroy() {
        translationHelper.close();
    }
}
