package id.ramdannur.kamus.ui.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ramdannur.kamus.R;
import id.ramdannur.kamus.entities.Translation;

/**
 * Created by Ramdannur on 1/22/2019.
 */
public class TranslationAdapter extends RecyclerView.Adapter implements Filterable {
    private final AdapterHelperListener listener;
    private List<Translation> data;
    private List<Translation> dataFiltered;
    private Context context;

    public TranslationAdapter(Context context, List<Translation> data, final AdapterHelperListener listener) {
        this.data = data;
        this.dataFiltered = data;
        this.listener = listener;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_translation, parent, false);
        vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            Translation movie = dataFiltered.get(position);
            ((ViewHolder) holder).tvTitle.setText(movie.getName());

            ((ViewHolder) holder).click(movie, listener);
        }
    }

    @Override
    public int getItemCount() {
        return dataFiltered.size();
    }

    public interface AdapterHelperListener {
        void onClick(Translation Item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title)
        TextView tvTitle;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void click(final Translation datum, final AdapterHelperListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(datum);
                }
            });
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    dataFiltered = data;
                } else {
                    List<Translation> filteredList = new ArrayList<>();
                    for (Translation row : data) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getName().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    dataFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataFiltered = (ArrayList<Translation>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
