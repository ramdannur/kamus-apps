package id.ramdannur.kamus.ui.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Ramdannur on 1/21/2019.
 */
public abstract class BaseFragment extends Fragment {

    // the root view
    protected View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = getView() != null ? getView() :
                inflater.inflate(getLayout(), container, false);
        return rootView;
    }

    protected abstract int getLayout();

}
