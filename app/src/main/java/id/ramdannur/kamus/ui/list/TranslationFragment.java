package id.ramdannur.kamus.ui.list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ramdannur.kamus.R;
import id.ramdannur.kamus.entities.Translation;
import id.ramdannur.kamus.helpers.Config;
import id.ramdannur.kamus.ui.base.BaseFragment;
import id.ramdannur.kamus.ui.detail.TranslationDetailActivity;

/**
 * Created by Ramdannur on 1/29/2019.
 */
public class TranslationFragment extends BaseFragment implements TranslationListener {
    private final String TAG = this.getClass().getSimpleName();
    private TranslationPresenter presenter;
    private TranslationAdapter adapter;
    private List<Translation> dataList;
    private String param;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    public static TranslationFragment newInstance(String param1) {
        TranslationFragment fragment = new TranslationFragment();
        Bundle args = new Bundle();
        args.putString(Config.KEY_ARGS, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            param = getArguments().getString(Config.KEY_ARGS, "");
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_translation;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, rootView);

        init();

        presenter = new TranslationPresenter(this, getContext());
        presenter.getData(param);
    }

    private void init() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        dataList = new ArrayList<>();

        adapter = new TranslationAdapter(getContext(), dataList,
                new TranslationAdapter.AdapterHelperListener() {
                    @Override
                    public void onClick(Translation item) {
                        startActivity(TranslationDetailActivity.getIntent(getActivity(), item.getId()));
                    }
                });

        recyclerView.setAdapter(adapter);
    }

    public void setEmptyList(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showTranslations(List<Translation> results) {
        if (dataList.size() == 0 && results.size() == 0) {
            setEmptyList(getString(R.string.empty_data));
        } else {
            dataList.clear();
            dataList.addAll(results);
            adapter.notifyDataSetChanged();
        }
    }

    public void filter(String keyword) {
        adapter.getFilter().filter(keyword);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}