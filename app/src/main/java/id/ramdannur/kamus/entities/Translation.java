package id.ramdannur.kamus.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ramdannur on 1/28/2019.
 */
public class Translation implements Parcelable {

    private int id;
    private String name;
    private String description;
    private String type;

    public Translation() {

    }

    public Translation(String name, String description, String type) {
        this.name = name;
        this.description = description;
        this.type = type;
    }

    public Translation(int id, String name, String description, String type) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.type);
    }

    protected Translation(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.description = in.readString();
        this.type = in.readString();
    }

    public static final Parcelable.Creator<Translation> CREATOR = new Parcelable.Creator<Translation>() {
        @Override
        public Translation createFromParcel(Parcel source) {
            return new Translation(source);
        }

        @Override
        public Translation[] newArray(int size) {
            return new Translation[size];
        }
    };
}
